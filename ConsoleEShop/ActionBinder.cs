﻿using BuisnessLayer;
using BuisnessLayer.DTO;
using BuisnessLayer.Enums;
using ConsoleEShop.DTO;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Linq;

namespace ConsoleEShop
{
    public class ActionBinder
    {
        
        private Dictionary<DelegateBinder, Delegate> actions;
        private UserDTO CurrentUser { get; set; }
        private ActionManager ac;
        public ActionBinder(Dictionary<DelegateBinder, Delegate> actions)
        {
            if (CurrentUser == null)
                CurrentUser = new UserDTO() { Role = Entity.Enums.UserRole.Guest };
            ac = new ActionManager();

            this.actions = actions;

            
        }
        private ActionBinder() { }
        private object[] UpdateOrderStatus() 
        {
            ShowHistory();

            if (CurrentUser.Orders?.Count() == 0)
                return new object[] { };

            Console.Write("Enter id of status: ");
            string orderId = Console.ReadLine();

            Console.Write("Enter new status: ");
            string newStatus = Console.ReadLine();
            return new object[]
            {
                new OrderChangeStatusByUserDTO()
                {
                    Id = orderId,
                    Status = (OrderStatus)Enum.Parse(typeof(OrderStatus), CultureInfo.CurrentCulture.TextInfo.ToTitleCase(newStatus.ToLower()))
                }
            };
        }
        private object[] EditProfile() 
        {
            Console.Write("Enter new name: ");
            string newName = Console.ReadLine();

            Console.Write("Enter new password: ");
            string newPassword = Console.ReadLine();

            string newRole = "";
            string email = "";

            if (CurrentUser.Role == Entity.Enums.UserRole.Admin) 
            {
                Console.Write("Enter new role: ");
                newRole = Console.ReadLine();

                Console.Write("Enter email: ");
                email = Console.ReadLine();
            }

            return new object[]
            {
                new UserUpdateDTO()
                {
                    Email = CurrentUser.Role == Entity.Enums.UserRole.Admin ? email : CurrentUser.Email,
                    Name = newName,
                    Password = newPassword,
                    Role = CurrentUser.Role == Entity.Enums.UserRole.Admin ? (UserRole)Enum.Parse(typeof(UserRole), CultureInfo.CurrentCulture.TextInfo.ToTitleCase(newRole.ToLower())) : UserRole.Registered
                }
            };
        }
        private void ShowProducts(object products) 
        {
            foreach (var product in products as IEnumerable<ProductDTO>)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"{product.Name}");
                Console.ResetColor();
                Console.WriteLine($"{product.Description}");
                Console.WriteLine($"{product.Price}");
                Console.WriteLine($"{product.Category}");
            }
        }
        private object[] SearchProduct() 
        {
            Console.Write("Enter name of product to search: ");
            string searchProductName = Console.ReadLine();
            return new object[] { searchProductName};
        }
        private void ShowProduct(object product) 
        {
            var tmp = (ProductDTO)product;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{tmp.Name}");
            Console.ResetColor();
            Console.WriteLine($"Product price: {tmp.Price}");
        }
        private object[] Register() 
        {
            Console.WriteLine("Enter email: ");
            string userEmail = Console.ReadLine();

            Console.WriteLine("Enter password: ");
            string userPassword = Console.ReadLine();

            Console.WriteLine("Enter name: ");
            string userName = Console.ReadLine();

            return new object[] { new UserRegisterDTO() {Email = userEmail, Name = userName, Password = userPassword } };
        }
        private object[] Login() 
        {
            Console.Write("Enter email: ");
            string userEmail = Console.ReadLine();

            Console.Write("Enter password: ");
            string userPassword = Console.ReadLine();

            return new object[]
            {
                new UserLoginDTO()
                {
                    Email = userEmail,
                    Password = userPassword
                }
            };
        }
        private object[] CreateNewOrder() 
        {
            this.ShowProducts(actions[DelegateBinder.ShowProducts].Method.Invoke(ac, null));

            OrderCreateDTO newOrder = new OrderCreateDTO() { UserEmail = CurrentUser.Email, ProductNames = new List<string>() };

            do
            {
                Console.Write("Enter product name (space to continue): ");
                string productName = Console.ReadLine();
                
                if (string.IsNullOrWhiteSpace(productName))
                    break;

                newOrder.ProductNames.Add(productName);

            } while (true);
            
            
            return new object[]
            {
                newOrder
            };
        }
        private void ShowAllUserInfo(object usersObj) 
        {
            var users = (IEnumerable<UserDTO>)usersObj;

            foreach (var user in users)
            {
                Console.WriteLine($"{user.Name}");
                Console.WriteLine($"Product price: {user.Email}");
            }
        }
        private object[] EditProduct() 
        {
            this.ShowProducts(actions[DelegateBinder.ShowProducts].Method.Invoke(ac, null));

            Console.Write("Enter product name to edit: ");
            string productName = Console.ReadLine();

            Console.Write("Enter new product description: ");
            string productDescr = Console.ReadLine();

            Console.Write("Enter new product category: ");
            string productCategory = Console.ReadLine();

            Console.Write("Enter new product price: ");
            string productPrice = Console.ReadLine();

            return new object[]
            {
                new ProductDTO()
                {
                    Name = productName,
                    Description = productDescr,
                    Category = productCategory,
                    Price = Convert.ToDecimal(productPrice)
                }
            };
        }
        private object[] AddProduct() 
        {
            Console.Write("Enter new product: ");
            string productName = Console.ReadLine();

            Console.Write("Enter new product description: ");
            string productDescr = Console.ReadLine();

            Console.Write("Enter new product category: ");
            string productCategory = Console.ReadLine();

            Console.Write("Enter new product price: ");
            string productPrice = Console.ReadLine();

            return new object[]
            {
                new ProductDTO()
                {
                    Name = productName,
                    Description = productDescr,
                    Category = productCategory,
                    Price = Convert.ToDecimal(productPrice)
                }
            };
        }
        private void ShowHistory() 
        {
            if (CurrentUser?.Orders?.Count() != 0)
            {
                foreach (var order in CurrentUser.Orders)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine($"Order id: {order.Id}");
                    Console.ResetColor();

                    foreach (var productInOrder in order.Products)
                    {
                        this.ShowProduct(productInOrder);
                    }

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Order status: {order.Status}");
                    Console.ResetColor();
                    Console.WriteLine(new string('▓', 20));
                }
            }
            else 
            {
                Console.WriteLine($"User {CurrentUser.Name} has no orders yet");
            }
            Console.ReadKey();
        }
        public Dictionary<string, Action> GetKeys()
        {
            if (CurrentUser == null)
                CurrentUser = new UserDTO() { Role = Entity.Enums.UserRole.Guest };

            Dictionary<string, Action> availableActions;

            availableActions = new Dictionary<string, Action>();

            availableActions.Add("Show Products", new Action(() => ShowProducts(actions[DelegateBinder.ShowProducts].Method.Invoke(ac, null))));
            availableActions.Add("Search Product", new Action(() => ShowProduct(actions[DelegateBinder.SearchProduct].Method.Invoke(ac, SearchProduct()))));

            if (CurrentUser.Role == Entity.Enums.UserRole.Guest)
            {
                availableActions.Add("Register", new Action(() => actions[DelegateBinder.Register].Method.Invoke(ac, Register())));
                availableActions.Add("Login", new Action(() => CurrentUser = (UserDTO)actions[DelegateBinder.Login].Method.Invoke(ac, Login())));
            }
            if (CurrentUser.Role == Entity.Enums.UserRole.Registered || CurrentUser.Role == Entity.Enums.UserRole.Admin)
            {
                availableActions.Add("Edit profile", new Action(() => actions[DelegateBinder.EditProfile].Method.Invoke(ac, EditProfile())));
                availableActions.Add("Update order status", new Action(() => 
                {
                    if (CurrentUser.Orders?.Count() != 0)
                        actions[DelegateBinder.UpdateOrderStatus].Method.Invoke(ac, UpdateOrderStatus());
                    else
                        Console.WriteLine("Current user has no orders");
                }));
                availableActions.Add("Create new order", new Action(() => actions[DelegateBinder.CreateNewOrder].Method.Invoke(ac, CreateNewOrder())));
                availableActions.Add("Logout", new Action(() => { CurrentUser = null; actions[DelegateBinder.Logout].Method.Invoke(ac, null); }));
                availableActions.Add("Show History", new Action(() => ShowHistory()));
            }
            if (CurrentUser.Role == Entity.Enums.UserRole.Admin) 
            {
                availableActions.Add("Add new product", new Action(() => actions[DelegateBinder.AddNewProduct].Method.Invoke(ac, AddProduct())));
                availableActions.Add("Edit product", new Action(() => actions[DelegateBinder.UpdateProduct].Method.Invoke(ac, EditProduct())));
                availableActions.Add("Show users info", new Action(() => ShowAllUserInfo(actions[DelegateBinder.ShowUserInfo].Method.Invoke(ac, null))));
                availableActions.Add("Edit user profile", new Action(() => actions[DelegateBinder.EditProfile].Method.Invoke(ac, EditProfile())));
                
            }
            return availableActions;
        }
    }
}
