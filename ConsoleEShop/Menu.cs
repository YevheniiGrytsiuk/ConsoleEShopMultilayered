﻿using BuisnessLayer;
using BuisnessLayer.DTO;
using BuisnessLayer.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata;
using System.Text;

namespace ConsoleEShop
{
    public class Menu
    {
        private Dictionary<DelegateBinder, Delegate> actions;
        private Dictionary<string, Action> methods;
        private ActionBinder ab;

        public Menu()
        {
            actions = new ActionManager().GetAvaialbleMethods();
            //methods = new ActionBinder(actions).GetKeys();
            ab = new ActionBinder(actions);
        }
        public void ShowAvaiableCommands()
        {
            methods = ab.GetKeys();

            int increment = 1;

            foreach (var menuItem in methods.Keys)
            {
                Console.WriteLine($"{increment++}. {menuItem}");
            }
            Console.WriteLine("0. Exit");
        }
        public void Method(int key) 
        {
            methods.ElementAt(--key).Value.Invoke();
        }
    }
}
