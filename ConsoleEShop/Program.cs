﻿using BuisnessLayer;
using System;
using System.Collections.Generic;

namespace ConsoleEShop
{
    public class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            
            int key = -1;

            do
            {
                menu.ShowAvaiableCommands();

                if (!(Int32.TryParse(Console.ReadLine(), out key)))
                {
                    Console.WriteLine("Wrong input!");
                    key = -1;
                }
                menu.Method(key);
                
                Console.ReadKey();
                Console.Clear();

            } while (key != 0);


        }
    }
}
